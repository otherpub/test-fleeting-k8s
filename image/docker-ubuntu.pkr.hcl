packer {
  required_plugins {
    docker = {
      version = ">= 0.0.7"
      source  = "github.com/hashicorp/docker"
    }
  }
}

source "docker" "ubuntu" {
  image  = "ubuntu:xenial"
  commit = true
}

variable "repo" {
  type = string
}

build {

  name = "test-fleeting-k8s"
  sources = [
    "source.docker.ubuntu"
  ]

  provisioner "file" {
    source = "../build/id_ed25519.pub"
    destination = "/tmp/authorized_keys"
  }

  provisioner "file" {
    source = "../build/test-fleeting-k8s"
    destination = "/usr/local/bin/test-fleeting-k8s"
  }



  provisioner "shell" {
    script = "setup.sh"
  }

  provisioner "file" {
    source = "../build/id_ed25519"
    destination = "/etc/gitlab-runner/id_ed25519"
  }

  # Custom build with https://gitlab.com/gitlab-org/fleeting/taskscaler/-/commit/ef086802e2d37a74ff6ff510693c8ceab2caa5f4
  provisioner "file" {
    source = "../build/gitlab-runner"
    destination = "/root/gitlab-runner"
  }

  post-processors {
    post-processor "docker-tag" {
      repository = var.repo
      tags = ["latest"]
    }
    # post-processor "docker-push" {}
  }
}
