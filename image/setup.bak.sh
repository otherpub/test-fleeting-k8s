#!/bin/bash

set -e
set -u

echo "Creating k8s user"
useradd -m k8s
mkdir -p /home/k8s/.ssh
chown k8s:k8s /home/k8s/.ssh
chmod 700 /home/k8s/.ssh
mv /tmp/authorized_keys /home/k8s/.ssh
chown k8s:k8s /home/k8s/.ssh/authorized_keys
chmod 600 /home/k8s/.ssh/authorized_keys

echo "Installing sshd"
apt-get update
apt-get install -y openssh-server rsyslog
mkdir -p /var/run/sshd
echo "AllowUsers k8s"  >> /etc/ssh/sshd_config

echo "Installing runner"
apt-get install -y curl
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
apt-get install -y gitlab-runner

echo "Installing tools for poking around locally"
apt-get install -y emacs-nox tmux
wget https://go.dev/dl/go1.21.4.linux-amd64.tar.gz
rm -rf /usr/local/go && tar -C /usr/local -xzf go1.21.4.linux-amd64.tar.gz
echo 'export PATH=$PATH:/usr/local/go/bin' >> /root/.profile
echo 'export PATH=$PATH:~/go/bin'          >> /root/.profile
mkdir -p /root/work
cd /root/work
GO=/usr/local/go/bin/go
git clone https://gitlab.com/otherpub/test-fleeting-k8s.git 
$( cd fleeting-plugin-k8s-demo/plugin ; $GO build )
git clone https://gitlab.com/gitlab-org/fleeting/taskscaler.git
$( cd taskscaler ; $GO build )
git clone https://gitlab.com/gitlab-org/gitlab-runner.git
$( cd gitlab-runner ; $GO build )
$GO install github.com/go-delve/delve/cmd/dlv@latest

