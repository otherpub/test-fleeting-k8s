#!/bin/bash

set -e
set -u

echo "Creating k8s user"
useradd -m k8s
mkdir -p /home/k8s/.ssh
chown k8s:k8s /home/k8s/.ssh
chmod 700 /home/k8s/.ssh
mv /tmp/authorized_keys /home/k8s/.ssh
chown k8s:k8s /home/k8s/.ssh/authorized_keys
chmod 600 /home/k8s/.ssh/authorized_keys

echo "Installing sshd"
apt-get update
apt-get install -y openssh-server rsyslog
mkdir -p /var/run/sshd
echo "AllowUsers k8s"  >> /etc/ssh/sshd_config

echo "Installing runner"
apt-get install -y curl
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
apt-get install -y gitlab-runner

echo "Installing tools for poking around locally"
apt-get install -y emacs-nox tmux

mkdir -p /root/work

#$GO install github.com/go-delve/delve/cmd/dlv@latest

