.PHONY :
build-image : check-env build-plugin generate-ssh-key
	cd image ; packer build \
	--var repo=${REPO} \
	docker-ubuntu.pkr.hcl

.PHONY : 
build-plugin :
	cd plugin ; go build \
	-o ../build/test-fleeting-k8s \
	.

.PHONY :
generate-ssh-key :
	yes | ssh-keygen -t ed25519 -C "k8s@localhost" -f build/id_ed25519 -N ""

.PHONY :
push-image : check-env
	docker push ${REPO}

.PHONY :
create-yolo :
	kubectl create clusterrolebinding yolo --clusterrole=admin --serviceaccount=default:default

.PHONY : check-env
create-runner :
	kubectl run -i --tty runner --image=${REPO}

.PHONY :
attach-runner :
	kubectl attach runner -c runner -i -t

.PHONY :
check-env :
ifndef REPO
	$(error Set REPO)
endif
