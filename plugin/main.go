package main

import (
	"context"
	"fmt"
	"time"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/gitlab-org/fleeting/fleeting/plugin"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

const (
	labelKey   = "app"
	labelValue = "test-fleeting-k8s"
)

var _ provider.InstanceGroup = &k8sDeployment{}

type k8sDeployment struct {
	clientset *kubernetes.Clientset
	settings  provider.Settings
	Repo      string
	Namespace string
}

/*
func (k *k8sDeployment) Init(ctx context.Context, logger hclog.Logger, settings provider.Settings) (provider.ProviderInfo, error) {
	if !settings.UseStaticCredentials {
		return provider.ProviderInfo{}, fmt.Errorf("this plugin cannot provision credentials")
	}
	if k.Repo == "" {
		return provider.ProviderInfo{}, fmt.Errorf("please provide repo in plug_config")
	}
	// config, err := rest.InClusterConfig()
	// if err != nil {
	// 	return provider.ProviderInfo{}, err
	// }
	var kubeconfig *string
	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()

	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		panic(err.Error())
	}
	k.clientset, err = kubernetes.NewForConfig(config)
	if err != nil {
		return provider.ProviderInfo{}, err
	}
	k.settings = settings
	return provider.ProviderInfo{
		ID:        "k8s",
		MaxSize:   100,
		Version:   "0.1.0",
		BuildInfo: "HEAD",
	}, nil
}*/

func (k *k8sDeployment) Init(ctx context.Context, logger hclog.Logger, settings provider.Settings) (provider.ProviderInfo, error) {
	if !settings.UseStaticCredentials {
		return provider.ProviderInfo{}, fmt.Errorf("this plugin cannot provision credentials")
	}
	if k.Repo == "" {
		return provider.ProviderInfo{}, fmt.Errorf("please provide repo in plug_config")
	}
	if k.Namespace == "" {
		return provider.ProviderInfo{}, fmt.Errorf("please provide namespace in plug_config")
	}
	config, err := rest.InClusterConfig()
	if err != nil {
		return provider.ProviderInfo{}, err
	}
	k.clientset, err = kubernetes.NewForConfig(config)
	if err != nil {
		return provider.ProviderInfo{}, err
	}
	k.settings = settings
	return provider.ProviderInfo{
		ID:        "k8s",
		MaxSize:   100,
		Version:   "0.1.0",
		BuildInfo: "HEAD",
	}, nil
}

func (k *k8sDeployment) Update(ctx context.Context, fn func(instance string, state provider.State)) error {
	pods, err := k.clientset.CoreV1().Pods(k.Namespace).List(ctx, metav1.ListOptions{
		LabelSelector: labelKey + "=" + labelValue,
	})
	if err != nil {
		return err
	}
	for _, p := range pods.Items {
		instance := p.Name
		switch p.Status.Phase {
		case corev1.PodPending:
			fn(instance, provider.StateCreating)
		case corev1.PodRunning:
			fn(instance, provider.StateRunning)
		default:
			fn(instance, provider.StateDeleting)
		}
	}
	return nil
}

func (k *k8sDeployment) Increase(ctx context.Context, n int) (int, error) {
	for i := 0; i < n; i++ {
		pod := &corev1.Pod{
			ObjectMeta: metav1.ObjectMeta{
				GenerateName: "worker-",
				Namespace:    k.Namespace,
				Labels: map[string]string{
					"app": "test-fleeting-k8s",
				},
			},
			Spec: corev1.PodSpec{
				Containers: []corev1.Container{
					{
						Name:  "worker",
						Image: k.Repo + ":latest",
						Ports: []corev1.ContainerPort{
							{
								Name:          "ssh",
								ContainerPort: 22,
							},
						},
						Command: []string{
							"/usr/sbin/sshd", "-D",
						},
					},
				},
			},
		}
		_, err := k.clientset.CoreV1().Pods(k.Namespace).Create(ctx, pod, metav1.CreateOptions{})
		if err != nil {
			return i, err
		}
	}
	return n, nil
}

func (k *k8sDeployment) Decrease(ctx context.Context, instances []string) ([]string, error) {
	for i, instance := range instances {
		err := k.clientset.CoreV1().Pods(k.Namespace).Delete(ctx, instance, metav1.DeleteOptions{})
		if err != nil {
			return instances[i:], err
		}
	}
	return nil, nil
}

func (k *k8sDeployment) ConnectInfo(ctx context.Context, instance string) (provider.ConnectInfo, error) {
	pod, err := k.clientset.CoreV1().Pods(k.Namespace).Get(ctx, instance, metav1.GetOptions{})
	if err != nil {
		return provider.ConnectInfo{}, err
	}
	expires := time.Now().Add(5 * time.Minute)
	return provider.ConnectInfo{
		ID:           instance,
		InternalAddr: pod.Status.PodIP,
		Expires:      &expires,
	}, nil
}

func main() {
	plugin.Serve(&k8sDeployment{})
}
